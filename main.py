import requests
from typing import Optional
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


@app.get("/is_vegan/{item_id}")
def isVegan(item_id):
    """tests if the article is vegan

    Args:
        item_id (int): id of the article

    Returns:
        bool: True is article is vegan, False otherwise
    """
    reponse = requests.get(
        'https://world.openfoodfacts.org/api/v0/product/' + str(item_id)
    ).json()
    ingredients = reponse["product"]["ingredients"]
    isVegan = all(ingredient["vegan"] == "yes" for ingredient in ingredients)
    return {"article": item_id, "vegan": isVegan}


if __name__ == "__main__":
    print(isVegan("3468570116601"))
    print(isVegan("3256540001305"))
    print(isVegan("7300400481588"))
