# TP5 - Conception logicielle

Ce projet a pour but de requeter l'API d'[Open Food Facts](https://world.openfoodfacts.org/)

## Initialisation du projet

```
git clone https://gitlab.com/alannagenin/open-food-facts.git
cd open-food-facts
pip install -r requirements.txt
```

# Lancer l'application

```
uvicorn main:app --reload
```
